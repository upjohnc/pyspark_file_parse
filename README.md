# Pyspark Coding Challenge: File Parsing

## Coding Challenge
The instructions are in `./dacs/instructions.txt`.<br />
The challenge is to parse an input file that
has rows of data that need to be combined.  The first three characters in the row are either `gaa` or `gab` and
the next four characters are an id made up of integer values.  The rows are matched on the id.  Then the
two rows are then concatenated together - the full rows.  Additionally, the id's indicate a language categorization.
The parsed rows are saved into their language category based on the `category.txt` file.

## Usage
- Place `input.txt` and `category.txt` into the s3 bucket under the prefix `coding-challenge`
- Place `process_file.py` in your s3 bucket in a prefix such as `code`.  Then run the Glue job you set up.
- The result is the output files will be saved in the s3 bucket with the format of `file{time}_{language}`

## Tech Stack
- pyspark on python 3.7
- Spark 2.4
- AWS S3
- AWS Glue 1.0

## Solution
- Parse the separate elements of each row (first three characters and then next four characters)
- create two dataframes - one for `gaa` and one for `gab`
- Join the two dataframes on id
- Concatenate the two "full" rows
- Filter on the category in the category file and then save to S3

## Tests
- *Usage*: run `make test` from root directory
- Tests were added for three of the functions
- It creates a local spark context
    - not sure if that is a good solution as there is a start up cost in creating the spark context for each session
- I used the pattern of having a `main` function in the script file called when calling the file as a script
    - This allowed me to move the spark context creation to not be called when importing the module - necessary for testing the functions
- I did think about having the logic functions moved into a `utils` file but then realized that I didn't know how to pass multiple files to `glue`

### Refactor of the parsing of the category file
- I wanted to test out the `mockrdd` package
    - this would decrease the need to have a spark context spin up on each run of test
- Set up a pytest marker for tests that don't use spark context
    - allow for continuous runs of these tests through a dev cycle (allow for more TDD like coding)
    - added commands in the makefile to run against the `no spark context` tests

## Glue Script
- script: `src/glue_job_creation_script.py`
- script to create the glue job and add a scheduled trigger
    - the script for some reason does not set the glue version and the python version correctly
        - when reviewing in the aws gui console the glue version and the python version aren't showing
