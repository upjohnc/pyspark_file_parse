# note need make >= 3.8.2
SHELL := bash
.ONESHELL:
.RECIPEPREFIX = >

root_dir = ./src


test:
> PYTHONPATH=$(root_dir) pytest -v

test-no-sc:
> PYTHONPATH=$(root_dir) pytest -v -m no_spark_context

test-watch:
> find . -type f -name "*.py" | entr env PYTHONPATH=$(root_dir) pytest -v

test-watch-no-sc:
> find . -type f -name "*.py" | entr env PYTHONPATH=$(root_dir) pytest -v -m no_spark_context

isort:
> isort -rc $(root_dir) .isort.cfg

mypy:
> mypy --ignore-missing-imports $(root_dir)
