from datetime import datetime
from functools import partial
from typing import Iterable, Tuple

from pyspark import RDD, SparkContext, SQLContext
from pyspark.sql import DataFrame, Row
from pyspark.sql.functions import col, concat

BASE_PREFIX = 'coding-challenge'


def language_parse(category_raw: RDD) -> Tuple[RDD, Iterable]:
    category_rdd = category_raw.map(lambda x: (x.split(',')[0].strip().lower(), x.split(',')[1].strip())).cache()
    languages = [i for i in category_rdd.map(lambda x: x[0]).distinct().collect()]
    return category_rdd, languages


def filter_by_language(raw_rdd: RDD, language_values: Iterable[str]) -> RDD:
    start_rdd = raw_rdd.map(lambda x: (x[0:3], x[3:7], x))
    language_rdd = start_rdd.filter(lambda x: x[1] in language_values)
    return language_rdd


def create_dataframe(sql: SQLContext, start_rdd: RDD) -> DataFrame:
    gaa = start_rdd.filter(lambda x: x[0] == 'GAA')
    gab = start_rdd.filter(lambda x: x[0] == 'GAB')
    gaa_df = sql.createDataFrame(gaa.map(lambda x: Row(first_a=x[0], second_a=x[1], last_a=x[2])))
    gab_df = sql.createDataFrame(gab.map(lambda x: Row(first_b=x[0], second_b=x[1], last_b=x[2])))
    join_me = gaa_df.join(gab_df, gaa_df.second_a == gab_df.second_b)
    return join_me


def combine_values(start_df: DataFrame) -> DataFrame:
    concat_df = start_df.select(concat(col("last_a"), col("last_b")).alias('fullname'))
    return concat_df


def save_values_s3(combined_values_df: DataFrame, language: str, timestamp: str) -> None:
    combined_values_df.write.text(f's3://{BASE_PREFIX}/file{timestamp}_{language}')


def process_language(sql_context: SQLContext, start_rdd: RDD, language: str, ids: Iterable[str],
                     timestamp: str) -> None:
    filtered_rdd = filter_by_language(start_rdd, ids)
    base_df = create_dataframe(sql_context, filtered_rdd)
    concat_df = combine_values(base_df)
    save_values_s3(concat_df, language, timestamp)


def run_task(language: str, sql_context: SQLContext, start_rdd: RDD, categories: RDD, timestamp: str) -> None:
    language_ids = categories.filter(lambda x: x[0] == language).map(lambda x: x[1]).collect()
    process_language(sql_context, start_rdd, language, language_ids, timestamp)


def logic_main(spark_context, sql_context):
    timestamp_base = datetime.utcnow().strftime("%Y%m%d%H%M%S")

    raw_file = spark_context.textFile('s3://{BASE_PREFIX}/input.txt').cache()

    category_file = spark_context.textFile('s3://{BASE_PREFIX}/category.txt').cache()
    category_rdd, languages = language_parse(category_file)

    run_task_language = partial(run_task, sql_context=sql_context, start_rdd=raw_file, categories=category_rdd,
                                timestamp=timestamp_base)
    list(map(run_task_language, languages))


def my_application_main():
    spark_context = SparkContext().getOrCreate()
    sql_context = SQLContext(spark_context)
    logic_main(spark_context, sql_context)


if __name__ == '__main__':
    my_application_main()
