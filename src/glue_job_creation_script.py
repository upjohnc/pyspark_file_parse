import os

import boto3

client = boto3.client('glue', aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'), )

job_name = 'pyspark-parsing'
base_prefix = 'coding-challenge'
script_location_prefix = s's3://{base_prefix}/code'
script_name = 'process_file.py'
script_location = f'{script_location_prefix}/{script_name}'

# create job
response = client.create_job(
        Name=job_name,
        Description='Parsing of file by language categories',
        Role='AWSGlueServiceRoleDefault',
        ExecutionProperty={
            'MaxConcurrentRuns': 3
            },
        Command={
            'Name': 'glueetl',
            'ScriptLocation': script_location,
            'PythonVersion': '3'
            },
        GlueVersion='1.0',
        NumberOfWorkers=1,
        WorkerType='Standard',
        )
print(response)

# create schedule
response = client.create_trigger(
        Name='nightly-run',
        Type='SCHEDULED',
        Schedule='cron(0 23 * * ? *)',
        StartOnCreation=False,
        Actions=[
            {
                'JobName': job_name,
                },
            ],
        )
