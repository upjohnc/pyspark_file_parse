from pathlib import Path

import pandas as pd
import pytest
from mockrdd import MockRDD
from pandas.testing import assert_frame_equal

import process_file


def assert_frame_equal_with_sort(results, expected, key_columns):
    results_sorted = results.sort_values(by=key_columns).reset_index(drop=True)
    expected_sorted = expected.sort_values(by=key_columns).reset_index(drop=True)
    thing = assert_frame_equal(results_sorted, expected_sorted, check_like=True)
    assert thing is None


@pytest.fixture(scope='module')
def input_data():
    present_dir = Path(__file__).parent
    input_dir = present_dir / 'input'
    with open(input_dir / 'input.txt') as f:
        read_data = f.read()
    input_data = [i for i in read_data.splitlines()]

    return input_data


@pytest.fixture(scope='module')
def category_data():
    present_dir = Path(__file__).parent
    input_dir = present_dir / 'input'
    with open(input_dir / 'category.txt') as f:
        read_data = f.read()
    category_data = [i for i in read_data.splitlines()]

    return category_data


@pytest.fixture(scope='module')
def category_data_no_case():
    present_dir = Path(__file__).parent
    input_dir = present_dir / 'input'
    with open(input_dir / 'category_case_mix.txt') as f:
        read_data = f.read()
    category_data = [i for i in read_data.splitlines()]

    return category_data


def test_filter_language(spark_context, input_data):
    """
    Test that filter and parser of the raw data functions correctly
    GIVEN: RDD of raw data having id's that match the filter and a few that do not match the filter
    WHEN: call the process_file.filter_by_language with desired id's plus one that doesn't match the 4 character id pattern
    THEN: returns just rows that match the passed id's and not the non-4 character id
    """
    # test that returns the three parts of the rdd
    # give many desired ids with input of more ids than from the desired language
    # added one that wasn't full four characters
    raw_rdd = spark_context.parallelize(input_data)
    result = [i for i in process_file.filter_by_language(raw_rdd, ('0013', '0014', '15')).collect()]
    # concern : order matters
    expected = [
        ('GAA', '0013', 'GAA001345ABCD12TY45.98'),
        ('GAB', '0013', 'GAB001367BVDC13TY45.98'),
        ('GAA', '0014', 'GAA001445ABCD12TY23.56'),
        ('GAB', '0014', 'GAB001467BVDC13TY23.56'),
    ]
    assert expected == result


def test_joined_dataframe(sql_context, spark_context):
    """
    Test that the two dataframes are joined on the id's correctly
    GIVEN: two tuples that have matching id's
    WHEN: call process_file.create_dataframe with the input data
    THEN: rows are joined properly on the id's
    """
    data = [
        ('GAA', '0013', 'GAA001345ABCD12TY45.98'),
        ('GAB', '0013', 'GAB001367BVDC13TY45.98'),
    ]
    result = process_file.create_dataframe(sql_context, spark_context.parallelize(data))
    result_pandas = result.toPandas()
    expected = pd.DataFrame({
        'first_a': ['GAA'],
        'second_a': ['0013'],
        'last_a': ['GAA001345ABCD12TY45.98'],
        'first_b': ['GAB'],
        'second_b': ['0013'],
        'last_b': ['GAB001367BVDC13TY45.98'],
    })
    assert_frame_equal_with_sort(result_pandas, expected, 'last_a')


def test_combining_two_strings(sql_context):
    """
    Test that the full string are concatenated together
    GIVEN: one row with a last_a and a last_b
    WHEN: call process_file.combine_values
    THEN: the two columns should be combined as a single string
    """
    # test the concatenating the two values
    input_data = pd.DataFrame({
        'last_a': ['GAA001345ABCD12TY45.98'],
        'last_b': ['GAB001367BVDC13TY45.98'],
    })
    result = process_file.combine_values(sql_context.createDataFrame(input_data)).toPandas()
    expected = pd.DataFrame({'fullname': ['GAA001345ABCD12TY45.98' + 'GAB001367BVDC13TY45.98']})
    assert_frame_equal_with_sort(result, expected, 'fullname')


@pytest.mark.no_spark_context
def test_category(category_data):
    """
    Test that the category file is parsed properly into a list of languages and an RDD made of a tuple of (language, id)
    GIVEN: an RDD of categories from a file
    WHEN: call process_file.language_parse
    THEN: a properly parsed category rdd and a list of languages
    """
    category_rdd = MockRDD.from_seq(category_data)
    result_rdd, languages = process_file.language_parse(category_rdd)
    expected_rdd = [
        ('java', '0013'),
        ('java', '0014'),
        ('python', '0017'),
        ('python', '0018'),
        ('spark', '0019'),
        ('spark', '0021'),
    ]
    assert expected_rdd == result_rdd.collect()
    assert sorted(languages) == sorted(['python', 'spark', 'java'])


@pytest.mark.no_spark_context
def test_category_language_case_independent(category_data_no_case):
    """
    Test that the languages in category file can be case independent
    GIVEN: an RDD of categories from a file with languages that are same but a mix of upper and lower case
    WHEN: call process_file.language_parse
    THEN: languages should be distinct
    """
    category_rdd = MockRDD.from_seq(category_data_no_case)
    result_rdd, languages = process_file.language_parse(category_rdd)
    assert sorted(languages) == sorted(['python', 'spark', 'java'])
